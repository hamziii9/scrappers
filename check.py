import pymysql

from flask import Flask, redirect, url_for, request , render_template
app = Flask(__name__)


@app.route('/torips',methods = ['POST', 'GET'])

def torips():

    error = ''
    try:
        conn = pymysql.connect(host='localhost', user='root', passwd='', db='hamza')
        x = conn.cursor()
        if request.method == "POST":

            user = request.form['ip']

            data = x.execute("select ip from tor_ips where ip = (%s)", (user))

            if data:
                return "The given IP is available"

            else:
                return "The given IP is not available. Try Again."
        else:

            user = request.args.get('ip')

            data = x.execute("select ip from tor_ips where ip = (%s)", (user))
            if data:
                return 'Your IP %s is present in the database' %user
            else:
                return 'Try Again'



        return render_template("torips.html", error=error)
        x.close()
        conn.close()

    except Exception as e:
        print(e)
        return render_template("torips.html", error=error)


if __name__ == '__main__':
   app.run()




